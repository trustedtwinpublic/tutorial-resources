# Trusted Twin meter manufacturer tutorial (Python)

This repository contains Jupyter notebooks for [Trusted Twin Python tutorials](https://trustedtwin.com/docs/tutorials/about.html).

## Requirements

Each tutorial requires:
- Python 3.6 or above.
- The [Trusted Twin Python library](https://trustedtwin.com/docs/libraries/library-python.html).
- The `json` library.
- The `Jupyter notebook`. 
     
You can install the Jupyter notebook using the pip package manager for Python:

```shell
pip install notebook
```

In order to run a notebook:

```shell
jupyter notebook
```

Please refer to the [Installing Jupyter](https://jupyter.org/install) website if more details regarding the installation are required.

## Copyright and license

Copyright 2023 Trusted Twin. All rights reserved.
     
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)
     
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.




